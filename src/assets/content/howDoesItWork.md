# App Functions

Visit the pins on the Rowntree Mills Park map to engage in From Weeds We Grow activities.

Tap or click on each pin to explore 360° panorama video/image animating stories by community members, experience video performances and teachings about the park, and view an interactive, co-created artwork.

> _If you’re on your smartphone, you will need the YouTube app to view these videos and they’ll move with you as you look around._
>
> _If you’re on a computer, you can click and drag to change your point-of-view._
>
> _Turn on the sound of your device to listen to stories and teachings rooted in the park. Closed captioning is available to turn on/off._
