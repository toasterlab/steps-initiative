# From Weeds We Grow

**Project Story**
_From Weeds We Grow_ was cultivated from conversations during a series of intergenerational workshops led by STEPS entitled, [_Under/Current_](https://stepsinitiative.com/project/under-current-mique-michelle-rexdale-mural/), that explored Rexdale’s connections to land, the Humber River, and the resilience of nature and community. _Under/Current_ culminated in a co-created mural on Finch Ave West produced in July 2019 by lead artist, Mique Michelle.
_From Weeds We Grow_ seeks to deepen the learnings from _Under/Current_ and respond directly to community feedback to strengthen the important connections between nature, accessible public space, art and wellness.

We have learned from local residents that Rowntree Mills Park is a community asset that has been underutilized for over a decade. _From Weeds We Grow_ aims to celebrate the park’s impact on community wellbeing, and encourages the enjoyment of the park as an inclusive and engaged public space to learn, create, and reconnect.

This project will also be featured as part of Myseum of Toronto INTERSECTIONS Festival in 2021.
