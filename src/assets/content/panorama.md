# Panorama Park

“Panorama Park has it all, beginning with a public area shaded by oak and walnut trees where local residents can gather at picnic tables to chat or participate in public events. The park is situated next to the Rexdale Community Hub, whose member agencies use the space for many of their regular programs.


Panorama Park also has a quiet woodlot which is a quiet place for walking or bird-watching. Its beating heart is the community garden, in which residents from local high-rise buildings get the opportunity to grow fresh vegetables and exercise mind and body. The garden has served for the past ten years as a focal point for local events such as children’s garden groups, agency programs and summer barbecues.


This park is also linked to the valley of the Humber River and Rowntree Mills Park, and as such is a gateway to long nature walks and many different recreational and cultural activities."


>-Jim Graham, former community garden designer/program developer, nature walk leader and native plant enthusiast.
