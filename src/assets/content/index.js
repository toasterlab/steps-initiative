import about from "assets/content/about.md";
import fromWeeds from "assets/content/fromWeeds.md";
import funderRecognition from "assets/content/funderRecognition.md";
import howDoesItWork from "assets/content/howDoesItWork.md";
import landing from "assets/content/landing.md";
import projects from "assets/content/projects.md";
import rowntree from "assets/content/rowntree.md";
import panorama from "assets/content/panorama.md";
import muralAbout from "assets/content/mural/about.md";
import jiyaPatel from "assets/content/mural/jiya-patel.md";
import milaZlatanovic from "assets/content/mural/mila-zlatanovic.md";
import ashleyBeerdat from "assets/content/mural/ashley-beerdat.md";
import grantEvers from "assets/content/mural/grant-evers.md";
import reyCastonguay from "assets/content/mural/rey-castonguay.md";
import billyParrell from "assets/content/mural/billy-parrell.md";
import jimGraham from "assets/content/mural/jim-graham.md";
import reitaT from "assets/content/mural/reita-t.md";
import alanaEvers from "assets/content/mural/alana-evers.md";
import fahmeedaQureshi from "assets/content/mural/fahmeeda-qureshi.md";
import shivaMoghaddasi from "assets/content/mural/shiva-moghaddasi.md";
import binaIsrani from "assets/content/mural/bina-israni.md";
import yasmanMehrsa from "assets/content/mural/yasman-mehrsa.md";

export {
  muralAbout,
  about,
  fromWeeds,
  funderRecognition,
  howDoesItWork,
  landing,
  projects,
  rowntree,
  panorama,
  jiyaPatel,
  milaZlatanovic,
  ashleyBeerdat,
  grantEvers,
  reyCastonguay,
  billyParrell,
  jimGraham,
  reitaT,
  alanaEvers,
  fahmeedaQureshi,
  shivaMoghaddasi,
  binaIsrani,
  yasmanMehrsa
};
