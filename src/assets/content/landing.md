# From Weeds We Grow

## Land Acknowledgement
We acknowledge the land we are meeting on is the traditional territory of many nations including the Mississaugas of the Credit, the Anishnabeg, the Chippewa, the Haudenosaunee and the Wendat peoples and is now home to many diverse First Nations, Inuit and Métis peoples. We also acknowledge that Toronto is covered by Treaty 13 with the Mississaugas of the Credit.
 
Visit [Native Land](https://native-land.ca/) to learn more about the lands you are on.Explore the park map and hear from community storytellers, spoken word artists and poets, view community-based art projects, and engage with these green spaces through self-guided walks and activities.

## Welcome
Explore the park map and hear from community storytellers, spoken word artists and poets, view community-based art projects, and engage with these green spaces through self-guided walks and activities. Share your web app experiences on social media using using [#growwithsteps](https://www.instagram.com/explore/tags/growwithsteps/?hl=en) and tagging [@STEPSpublicart](https://www.instagram.com/stepspublicart/). Enjoy your visit!

## What's New
Join us for upcoming From Weeds We Grow activities and check out new community poems and artworks, added monthly through summer 2022. Join us for monthly Arts in the Parks 2022 events in Panorama Park this summer! [Reserve your spot here.](https://stepspublicart.org/register-today-from-weeds-we-grow-2022/)

## App Functions

Visit the pins on the Rowntree Mills Park and Panorama Park map to engage in _From Weeds We Grow_ activities.

Tap or click on each pin to explore 360° panorama video/image animating stories by community members, experience video performances and teachings about the park, and view an interactive, co-created artwork.

> _If you’re on your smartphone, you will need the YouTube app to view these videos and they’ll move with you as you look around._
>
> _If you’re on a computer, you can click and drag to change your point-of-view._
>
> _Turn on the sound of your device to listen to stories and teachings rooted in the park. Closed captioning is available to turn on/off._
