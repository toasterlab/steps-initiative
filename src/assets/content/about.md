## From Weeds We Grow

From Weeds We Grow is an interdisciplinary community-based public art project led by STEPS Public Art that explores Rowntree Mills and Panorama Parks through artistic, Indigenous, and community-based approaches.

Since 2020, From Weeds We Grow is an interdisciplinary public art project that aims to encourage exploration, and conversation around public space, while connecting community members to each other, to the arts, and to public greenspaces. This project was cultivated through conversations during the production of [_Under/Current_](https://stepsinitiative.com/project/under-current-mique-michelle-rexdale-mural/), another STEPS interdisciplinary program in 2019 that transformed the Rexdale community through co-created public art and storytelling.

Part of Arts in the Park, this project is supported by the Toronto Arts Council through the Animating Toronto Parks program that has led to the development of an interactive web app, free activity booklets, participatory art-making and storytelling workshops, and community events led by local community members, artists and facilitators.

## STEPS

STEPS Public Art (STEPS) is a Canadian-based public art organization that develops one-of-a-kind art initiatives and engagement strategies that transform public spaces. We are responsible for Canada’s largest and most community-engaged public art initiatives.

Together with our partners, we transform urban areas into vibrant public spaces, helping artists, community organizations, BIAs, and developers push creative and technical boundaries - breathing new artistic energy into our public spaces.

For more information on STEPS or to get in touch, visit [stepspublicart.org](https://stepspublicart.org/) and follow [@STEPSpublicart](https://www.instagram.com/stepspublicart/).

## Contributors

**Lindsey Lickers**

> [Lindsey Lickers, Mushkiiki Nibi (Medicine Water)](http://lindseylickers.ca/), is Turtle Clan originally from Six Nations of the Grand River, with matriarchal ties to the Mississauga’s of Credit First Nation. She has been a practicing artist specializing in painting, beading and community arts facilitation going on fifteen years. In 2019, Lindsey was awarded an International Women’s Day Award (City of Toronto) recognizing her unique synthesis of public art, Indigenous women’s issues and governance.

**Tracey Kayy**

> Tracey Kayy is a live performer based in Toronto who has performed at over 100 events within the last year alone. She has performed at the Toronto City Hall, Brampton City Hall, and received appreciation from the Mayor of Mississauga for consistently serving her community. She is a singer, song-writer, producer, rapper and spoken-word artist. Her creative creations mostly shed light on the difficulties and traumas within our society - Realities which are often ignored are brought to light by her musical compositions. She is a radiant light, and a messenger who raises awareness by being a voice for multiple communities across the globe. Her soul’s purpose is to heal people from all walks of life through her writings and musical creations.

**Zara Rahman**

> [Zara Rahman](https://www.instagram.com/zaratrahman/?hl=en) is a spoken word artist dedicated to the craft of creative writing and storytelling. Through written and spoken word poetry, Zara has shared stories with audiences up to 2000 and in more intimate settings. The Toronto based artist holds numerous awards and publications in local competitions and virtual settings. She is also the founder of a nonprofit organization called [Youth Professionals](https://www.instagram.com/youthprofessionals/?hl=en), dedicated to BIPOC and LGBTQ+ youth and their future careers. Zara’s goal is to not only teach the craft of creative writing, but to pass down the courage required to express oneself in new and exciting forms.

**James Carpenter**

> James Carpenter, Grey Cloud, is a recognized Traditional Healer. He has mixed Ojibwa and Cree and Chippewa ancestry from James Bay, Chippewa of the Thames, and Alderville, Ontario. He speaks English as well as some Ojibwa and Cree. Since 2003, James has worked as an Oshkabewis (helper) at Anishnawbe Health Toronto under the tutelage of various Traditional Healers in the Traditional Healing Services Program. He has gained the respect and knowledge of various healers and has progressed to the point of using his gifts with the purpose of helping our communities heal. James has an ability to connect and work with the ancestors and spirit helpers. Working with youth and their families is an area of strength which has become an integral component of community healing.

**FOURONESIXLOVE INC.**

> Led by Ms. Ellie Ricketts, [FOURONESIXLOVE INC.](https://www.instagram.com/fouronesixlove/?hl=en) is driven on supporting the development of the community through curating special events in the Humber Summit area and beyond.

**Adassa B**

> Adassa is a community leader and collaborator for the From Weeds We Grow project. Adassa leads community walks through Rexdale and Humber Summit, supporting her friends and neighbors in exploring North Etobicoke’s natural green spaces.

**Quentin VerCetty**

> Winner of the 2020 Joshua Glover Memorial competition (Toronto), [Quentin VerCetty](https://www.vercetty.com/) is a multi-award-winning multidisciplinary storyteller, educator, and Afrofuturist. With a Bachelor's in Fine Arts from OCAD University and Master's in Art Education from Concordia University, his work speculates inclusive public spaces and has exhibited his work around the world.
Community Storytellers and Poets. 

**Caity Ferrar**

> [Caity Ferrar](https://www.instagram.com/missferrar/) was born and raised in Brantford, Ontario and started taking art classes at a young age, with her father. Encouraged by her family, she attended  Sheridan College and OntarioCollege of Art and Design University. Calling Toronto home for 7 years, she created and exhibited work, facilitated artist workshops, murals and other large pieces as a contract artist. Caity and her son Oliver, proudly call Paris their home. Spirituality is an essential catalyst in Caity’s life and her creative process. She allows the paint to drip and flow intuitively to let the pieces come together organically. This allows the unconscious to become conscious, with different energy and life to each piece. Her ability to see the flowing energy and its interaction with the elements breathes life into her creations.

**Matthew Fountain**

> Matthew Fountain is an Ojibwe born and raised in Toronto. His character was shaped by the teachings shared by his mother and father. With an emphasis on kindness, diligence, and gentleness his parents raised him to be a person to value sharing and helping others. It was these simple lessons which led him to become an Oshkabewis (ceremonial helper) 

**Kanisha Dabreo**

> [Kanisha Dabreo](https://www.instagram.com/artisticperceptionca/) is a Caribbean-Canadian multidisciplinary artist and digital designer based in the GTA. Artistic Perception is a creative company that provides fine art, prints, and cust@artistiperceptioncaom pieces/designs.

**Kareen Weir**

> [Kareen Weir](https://www.instagram.com/odd0ne/) is a multidisciplinary artist dedicated to exploring and preserving Jamaican Patwah through her arts practice. She is a graduate of the Edna Manley College with a major in Sculpture, but has been exploring the world of mural painting since immigrating to Canada. Her work looks at themes of memory, retention, representation and identity through the Black experience and deliberate use of Patwah language as the primary means of expression. 

**Marina Fathalla**

> [Marina Fathalla](https://www.instagram.com/_nous_nous/?hl=en) is a multidisciplinary artist, educator and collaborator whose work examines intersections of poetics and politics. Since 2021, Majan has supported land-based art-making and thoughtful park and movement explorations with the From Weeds We Grow team. 

**Marjan Verstappen**

> [Marjan Verstappen](https://www.marjanverstappen.com/) is a multidisciplinary artist and facilitator whose art practice encompasses installation, drawing, photography and performance. Since 2021, Majan has supported land-based art-making and thoughtful park and movement explorations with the From Weeds We Grow team. 

**Community Storytellers and Poets**

> Abba Wie-Addo  
> Bina Israni  
> Domenica Venir  
> Ellie Ricketts  
> Grant Evers  
> Ima-Obong  
> Martha Esin  
> Nishtha (Nish) Israni  
> Pathma Karunaratnarajah  
> Richa Baghel  
> Warda Sharmeen

**Toasterlab**

> Toasterlab creates place-based extended reality experiences that promote deeper engagement with history, community, and imagination. Toasterlab combines expertise in storytelling, theatrical and media production, and the development of new technology to produce both original work and partnerships. Our work ranges from site-specific live performance to bespoke mobile applications and VR films, and often combines a variety of approaches to collapse time and space for the delivery of impactful narratives. From the largest festivals to community youth workshops, we build accessible ways to understand places in whole new ways. Toasterlab is Ian Garrett, Justine Garrett, and Andrew Sempere.

