# Projects

## Rowntree Mills Park Activity Booklet

A collection of environmental, art and cultural, and placemaking activities suitable for the whole family designed by Melting Pot Collective [(@mpctoronto)](https://www.instagram.com/mpctoronto/) and collaborating artists.

>  [2020 Activity Booklet](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/a426c56c-5802-42b5-903c-ea786c2cd4d8.pdf) by Melting Pot Collective

>  [2021 Activity Booklet](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/c95c9e6f-00f2-4a69-be74-d379fbec94ae.pdf) designed by Melting Pot Collective, Debbie Woo [(@woohyunji)](https://www.instagram.com/woohyunji/), Marjan Verstappen [(@far_stepping)](https://www.instagram.com/far_stepping/) and Marina Fathalla [(@_nous_nous)](https://www.instagram.com/_nous_nous/).

>  [2022 Activity Booklet](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/75991308-cf38-4113-8d9f-2f251c6edde4.pdf) designed by Debbie Woo [(@woohyunji)](https://www.instagram.com/woohyunji/) and Jesseca Buizon [(@jayymadethis)](https://www.instagram.com/jayymadethis/).

## Art of Rowntree Mills Park

### _Change_, a community co-created [virtual collage](/mural) led by Lindsey Lickers

An online art workshop for the community to gather and explore visual art and symbolism that represents their stories culminating in a collective, interactive artwork.

**Participating Artists**  
 Alana Evers  
 Ashley Beerdat  
 Billy Parrell  
 Bina Israni  
 Fahmeeda Qureshi  
 Grant Evers  
 Jim Graham  
 Jiya Patel  
 Mila Zlatanovic  
 Reia Tariq  
 Rey Castonguay  
 Shiva Moghaddasi  
 Yasaman Mehrsa

### _Contributing to the Bundle - Restoration of Relationship_

An interdisciplinary arts workshop exploring the significance of water and our connections to the environment through the creation of beaded artworks and public art that reflect our commitments to the land.

Workshop led by Lindsey Lickers [(@mushkiiki_water)](https://www.instagram.com/mushkiiki_water/) in partnership with [Ontario Culture Days](https://culturedays.ca/en/on)

Ravine Ground Mural led by Lindsey Lickers in partnership with [Park People](https://parkpeople.ca/) InTO the Ravines program.

## Poetry of Rowntree Mills Park

### Performances by Tracey Kayy and Zara Rahman

Two local spoken word artists perform original work rooted in their lived experience in Rowntree Mills Park.

### Community Poetry Workshops led by Zara Rahman

A thematic online workshop series exploring a range of literary devices to create park-based poetry.

## Stories of Rowntree Mills Park: Presented by community storytellers

Community storytellers share their memories, experiences in Rowntree Mills Park, and thoughts on its future through audio recordings.

## Walks of Rowntree Mills Park

A medicine walk and teachings with James Carpenter about the interconnectedness of the trees, plants, wildlife, and the Humber River.

_A Stroll Through Time_ is a park walk led by local resident, Ellie Ricketts, and her children about the history and potential futures of the park.
