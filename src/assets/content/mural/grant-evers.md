# Grant Evers

![Grant Evers](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/6748bc82-5840-4530-9f5e-3fe42e62372c.jpeg)

New Growth from Stump of Tree Cut Down – A Symbol of Life Rediscovered
