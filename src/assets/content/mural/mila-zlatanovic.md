# Mila Zlatanovic

![Mila Zlatanovic](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/de4d91df-db71-4e4a-b9b0-c5ebe69243c4.jpeg)

_Fish in the Sky_

Always being a child of two,  
two different landscapes,  
different cultures,  
but being born and raised in a third,  
and still, belonged nowhere.

This image represents me  
and my roots.  
My constant inner search,  
rolling out of water,  
swimming into the sky.

Detached in a sense,  
or closer to myself than ever.
