# Fahmeeda Qureshi

![Fahmeeda Qureshi](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/002dbfdc-6e3c-4567-b4ae-995d9411c6f5.jpeg)

I was not enjoying nature for a long time - busy with my life.  
Then I connected to nature and I learned from nature to care for myself.  
Now I am enjoying nature and my life along with helping my community.
