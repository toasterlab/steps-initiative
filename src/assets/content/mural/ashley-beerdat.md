# Ashley Beerdat

![Ashley Beerdat](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/5f2b3901-3695-485a-9711-453eb6700101.jpeg)

The tree symbolizes life, growth, change and transition. It reminds us that our creative journey is not a straight path but branches in different directions. With each pivot of growth, a new lesson is learned. As we grow, our roots support us in experiencing new change providing wisdom and knowledge to take on the next chapter of our journey.
