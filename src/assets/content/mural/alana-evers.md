# Alana Evers

![Alana Evers](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/9c4b4823-18ba-4a02-8695-7b53bd380a46.jpeg)

_Salmon Run on the Humber_

Growing up along the banks of the Humber River, transformation occurs every year through the change of the seasons. My favourite season is autumn, when the leaves turn, and the land lets go of the summer thrum to ready itself for the cold and quiet winter ahead. Salmon can be seen swimming north along the river at this time of year to spawn. Their tenacity inspires me, as they swim upstream against the cold current, navigating the rocky water and jumping the falls as they go. Against all odds, they persevere. The salmon remind me that we too can persist through difficult waters.

In historical Christian art, golden halos were often depicted around the heads of holy people to accentuate their holiness. It’s not a conscious decision on my part, but when I create art intuitively I often find myself surrounding my subjects with a glowing light. For me, it can represent an outpouring of positive energy, joy, or sacred connection. I have an inherent reverence for nature, and wanted to honour the salmon with this piece.
