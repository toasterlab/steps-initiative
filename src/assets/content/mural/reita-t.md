# Reia T

![Reia T](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/1629b0de-8732-4687-8c71-c7171b9ec163.jpeg)

_Paisley Constellation_

In a reflection of my own Afghan heritage, I decided to paint paisley’s across the night sky, with them looking more abstract/bacteria-ish in a way just to reflect how primordial space can be. Various more realistic looking stars are scattered around as well as little circles to reflect how messy even life above us is but also a little representation of the Afghan diaspora scattered across the world.
