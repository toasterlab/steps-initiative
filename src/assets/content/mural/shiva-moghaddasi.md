# Shiva Moghaddasi

![Shiva Moghaddasi](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/4f833124-5cb2-4ad3-8fae-18f61fe257f2.jpeg)

The story of Phoenix resonates deeply with me. In Persian mythology, Phoenix is the most influential and mentioned bird. It lives for decades, and when it gets old, it builds a big nest and sits there until the nest catches on fire. The bird dies in the great fire, and from the ashes rises the young phoenix. Phoenix was also considered a lucky bird. When a king would pass away without an heir, the phoenix would fly down from the mountains and would circle over the city until it would sit on someone’s head, and that person (chosen by the phoenix) would become the new king.

The death of an old phoenix gives birth to the young phoenix. The new life raised from death in such a dramatic way tells me that for a real change into a better self one needs to let go and detach oneself from the world and that entire one holds dear (even it means life itself). Only in letting go without fear one can find the ultimate existence through freeing their spirit. This is also a philosophy of Rumi (a 13th century Persian philosopher & poet).

The 2020 pandemic and several months of lockdown time forced me to pause my busy life. I started pondering where my life was going and whether I am on the right path - and most importantly what is the right path? I started reading Rumi’s poems, and his philosophy makes sense to me now. I needed to stop, look deep within myself and get to know the real me again. I’ve started my new journey. I’m shedding some old habits, especially what’s been fed to me through the consumerism materialistic lifestyle of North America.

Your _From Weeds We Grow_ project resonated with my phoenix spirit. I feel the change and embrace it. It’s time for the young phoenix to rise.
