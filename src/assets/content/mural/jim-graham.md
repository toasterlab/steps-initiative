# Jim Graham

![Jim Graham](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/ab24c8dd-598d-4e9c-8e17-12b5cb59fda2.jpeg)

A native plant that is uncommon in Ontario, Virginia Mallow (Sida hermaphrodita). It is an herbaceous perennial that grows to about 1 metre in height.

The sequence of photos represents the change from summer (in flower) through the autumn (leaf beginning to change colour followed by abscission in a couple of weeks). The final picture is of the seedheads, arranged in the classic “cheese” shape that is characteristic of mallows.

This also represents part of my personal growth in native plant identification and knowledge. I found this plant (then about 7cm tall) sulking in a small pot, and took it based on a hunch, mainly because of the fantastic leaf shape.

The plant community in Rowntree Mills park is badly impacted by non-native species, and this is one plant (of many) that I would particularly love to see occupying a certain wet spot down by where the toads hang out near the south end of the park.
