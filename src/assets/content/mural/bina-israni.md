# Bina Israni

![Bina Israni](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/fe42704f-1eea-4a00-a0eb-d715f19b333f.jpeg)

So much feelings I have for Mother Earth. I had a very blessed childhood to be able to live among the trees -- lots of greenery, blue skies, surrounded by hills and mountains sparkling and radiating sunlight.

Trees are our eternal friends. They store thousands of stories, feelings and experiences of generations after generations in their memories.

Nature is our best teacher. So perfectly it blends with other living and non-living things coexisting beautifully.

We all are definitely an integral part of Mother Earth. Many times on my walk I hug and talk to the trees. I feel the divine presence among them.

Without the Sun nothing can exist, without trees we cannot live.
