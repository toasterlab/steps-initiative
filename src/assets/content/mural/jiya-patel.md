# Jiya Patel

![Jiya Patel](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/5a2392b1-cebc-4a62-9f46-d7d4dfedf587.jpeg)

My connection to Rowntree Mills Park is the nature and animals. My artwork is a riotous colour of fall leaves and majestic creatures. This piece is in honour of my pet bird, Chico, who passed away in the summer.
