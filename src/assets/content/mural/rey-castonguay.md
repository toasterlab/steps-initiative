# Rey Castonguay

![Rey Castonguay](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/f9e2bc08-884d-4355-842c-81f476ecbea3.jpeg)

_Where I’m from_

A tide pool —  
Where the ocean meets the land  
moss beds grounded with the ebb and flow.

The moon and the earth  
pulling and pushing  
salt water as medicine.

Where the sunlight warms rocks built up in many layers  
tracing the coastline  
and gentle streams moving through forests  
along a wooded path of recollection and deep knowing.
