**_Change_**

Led by Artist, Lindsey Lickers, ‘Mushkiiki Nibi’ (Medicine Water), this digital collage was done in collaboration with local community members surrounding the Rowntree Mills Park, inspired by one interpretation of the fancy shawl dance and supported by the teaching of a butterfly’s metamorphosis provided by James Carpenter.

Participants were asked to reflect on the overarching theme of transformation and create a visual representation of how they are moving through change in their current experience. The works were then combined with a painting of a fancy shawl dancer, with the individual expressions becoming the dancer’s regalia.
