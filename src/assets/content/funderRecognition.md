# Funder Recognition

From Weeds We Grow is part of the Arts in the Parks program. It has been made possible through generous support from

![CCA](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/f828f61c-8fb8-4d3c-89c7-4a976fe3c5c7.png)

We acknowledge the support of the Canada Council for the Arts

![CCA](https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/f579be71-257f-484d-bf34-aea3ced8bcff.png)