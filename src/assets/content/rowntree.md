# Rowntree Mills Park 

With 227 acres of wildlife and the Humber River flowing through, Rowntree Mills Park is a wonderful place to escape from the busy city streets. This park has a vibrant history that is largely unknown to many of today’s residents living in the Humber Summit community.

This beautiful space is well-maintained by City parks staff, consists of trails, bridges, picnic areas and nature at its finest.

Joseph Rowntree was a pioneer in the village of Thistletown. Starting in 1843, he began building a sawmill and a grist mill on the banks of the Humber River; they became known as the “Greenholme Mills”, which operated profitably until the end of the 19th Century. In 1927, the Finnish Society of Toronto, established a summer retreat for Finnish workers, which they named Tarmola. It was located at the north end of present-day Rowntree Mills Park. Volunteers built a large recreational complex there including athletic facilities, an open-air dance pavilion, a band shell, a sauna and a restaurant. During the 1930s at a time when there were not many comparable tracks, many Canadian Olympians trained at Tarmola, which was accessed by a dirt road from where Steeles Ave. ended at Islington Ave. The site regularly hosted Finnish-Canadian cultural and athletic festivals. As many as 3000 people would attend performances featuring choirs and athletes representing as many as 77 sports clubs from Quebec to British Columbia.

Today, the park is widely utilized by the community members and residents surrounding the Humber River for bike rides, walks, hikes, bird watching, deer spotting, fishing, picnics, small gatherings and much much more. A beautiful hidden Gem of a park in the Rexdale community, proudly one of Toronto's largest!

The park means a lot to me and my family. Its open space is a great way to get some exercise of the mind and body. I appreciate nature so much more enjoying a stroll through Rowntree Mills Park.

>Ellie Ricketts, Fouronesixlove Inc. [@fouronesixlove](https://www.instagram.com/fouronesixlove/?hl=en)


Rowntree Mills Park forms part of the Humber River. Established in 1959, it was originally known as Riverbend Park and is a beautiful expanse of greenspace, it is a huge park - one of the largest in Toronto. The park is located in North Etobicoke, a diverse community which is also a Neighbourhood Improvement Area.

The park is great for hosting many different events and activities - Guided nature walks, bird and deer watching, picnics, cycling and art. The diversity of native species and learning about the ravine system are just a few of the amazing things the park is used for. 

The park's greenspace is well maintained by the City Parks staff and contractors. I love walking and exploring the park with my colleagues. We have been to different greenspaces and love the 2 huge ponds that house many birds and other wildlife. The native flowering plants is a must see on anyone's list.

>Adassa B., Community Volunteer and Park People Walk Leader
