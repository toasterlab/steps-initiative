import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home, Mural } from "pages";

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/mural">
            <Mural />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
