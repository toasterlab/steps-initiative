import CONST from "constants.js";

export const makeAssetURL = (url) => {
  if (!url || url.includes("http")) {
    return url;
  } else {
    return `https://assets.toasterlab.com/8edaca6c-98dc-4106-9d7e-a2b2119c9ea6/${url}`;
  }
};

export const detectEnvironment = () => {
  let userAgent = navigator.userAgent || navigator.vendor || window.opera;
  if (/android/i.test(userAgent)) return CONST.PLATFORM.ANDROID;
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)
    return CONST.PLATFORM.IOS;
  return CONST.PLATFORM.STANDARD;
};
